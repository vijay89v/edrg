FROM openjdk:8-jdk-alpine

ADD target/hello-docker-0.0.1-SNAPSHOT.jar hello-docker.jar

EXPOSE 9080

ENTRYPOINT ["java","-jar","hello-docker.jar"]